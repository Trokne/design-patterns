﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.DesignPatternCase
{
    public static class DesignPatternRunner
    {
        public static void Run()
        {
            Console.WriteLine("Выберите паттерн проектирования:");
            var patternCases = GetPatternCases();
            PrintPatterns(patternCases);
            var index = ReadPatternIndex(patternCases.Length);
            patternCases[index].Run();
        }

        private static int ReadPatternIndex(int count)
        {
            Console.Write("Номер паттерна: ");
            
            if (!int.TryParse(Console.ReadLine(), out var number)
                || number < 0 
                || number >= count)
            {
                Console.WriteLine("Ошибка ввода");
                ReadPatternIndex(count);
            }

            return number;
        }

        private static void PrintPatterns(IReadOnlyList<IDesignPatternCase> patternCases)
        {
            for (var i = 0; i < patternCases.Count; i++) 
                Console.WriteLine($"{i}: {patternCases[i]}");
        }

        private static IDesignPatternCase[] GetPatternCases()
        {
            var genericInterface = typeof(IDesignPatternCase);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => genericInterface.IsAssignableFrom(p) && !p.IsInterface)
                .Select(t => (IDesignPatternCase)Activator.CreateInstance(t))
                .ToArray();
        }
    }
}