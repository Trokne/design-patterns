﻿namespace DesignPatterns.DesignPatternCase
{
    public interface IDesignPatternCase
    {
        /// <summary>
        /// Запускает пример использования паттерна проектирования
        /// </summary>
        void Run();
    }
}