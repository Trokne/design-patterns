﻿using StructuralPatterns.Decorator;

namespace DesignPatterns.DesignPatternCase.Structural
{
    public class DecoratorCase : IDesignPatternCase
    {
        public void Run()
        {
            var notifier = new SlackNotifier() as BaseNotifierDecorator;
            notifier = new TelegramNotifier(notifier);
            notifier = new VkNotifier(notifier);
                
            notifier.Notify();
        }

        public override string ToString() => "Декоратор";
    }
}