﻿using System;
using StructuralPatterns.Proxy;

namespace DesignPatterns.DesignPatternCase.Structural
{
    public class ProxyCase : IDesignPatternCase
    {
        public void Run()
        {
            IUniversity university = new CachedUniversity(new University());
            
            Console.WriteLine();
            for (var i = 1; i <= 3; i++)
            {
                Console.WriteLine($"Вызов {i}");
                Console.WriteLine($"{GetFaculties(university)}\n");
            }
        }

        private string GetFaculties(IUniversity university) =>
            $"Факультеты: {string.Join(", ", university.GetFaculties())}";

        public override string ToString() => "Прокси";
    }
}