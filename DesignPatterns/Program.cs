﻿using DesignPatterns.DesignPatternCase;

namespace DesignPatterns
{
    internal static class Program
    {
        private static void Main() => DesignPatternRunner.Run();
    }
}