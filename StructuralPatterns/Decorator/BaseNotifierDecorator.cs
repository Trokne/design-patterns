﻿namespace StructuralPatterns.Decorator
{
    public abstract class BaseNotifierDecorator : INotifier
    {
        private readonly BaseNotifierDecorator _mainNotifier;
       
        protected BaseNotifierDecorator() { }
        
        protected BaseNotifierDecorator(BaseNotifierDecorator mainNotifier)
        {
            _mainNotifier = mainNotifier;
        }

        public virtual void Notify()
        {
            _mainNotifier?.Notify();
        }
    }
}