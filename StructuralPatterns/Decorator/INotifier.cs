﻿namespace StructuralPatterns.Decorator
{
    public interface INotifier
    {
        void Notify();
    }
}