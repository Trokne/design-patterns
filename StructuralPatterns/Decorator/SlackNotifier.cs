﻿using System;

namespace StructuralPatterns.Decorator
{
    public class SlackNotifier : BaseNotifierDecorator
    {
        public SlackNotifier() { }
        
        public SlackNotifier(BaseNotifierDecorator mainNotifier) 
            : base(mainNotifier) { }

        public override void Notify()
        {
            base.Notify();
            Console.WriteLine("Уведомил людей в Slack");
        }
    }
}