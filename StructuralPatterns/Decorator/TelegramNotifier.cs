﻿using System;

namespace StructuralPatterns.Decorator
{
    public class TelegramNotifier : BaseNotifierDecorator
    {
        public TelegramNotifier() { }
        
        public TelegramNotifier(BaseNotifierDecorator mainNotifier) 
            : base(mainNotifier) { }

        public override void Notify()
        {
            base.Notify();
            Console.WriteLine("Уведомил людей в Telegram!");
        }
    }
}