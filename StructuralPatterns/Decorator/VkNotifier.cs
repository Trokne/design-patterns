﻿using System;

namespace StructuralPatterns.Decorator
{
    public class VkNotifier : BaseNotifierDecorator
    {
        public VkNotifier() { }
        
        public VkNotifier(BaseNotifierDecorator mainNotifier) 
            : base(mainNotifier) { }

        public override void Notify()
        {
            base.Notify();
            Console.WriteLine("Уведомил людей в Vk");
        }
    }
}