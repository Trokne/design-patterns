﻿using System;
using System.Collections.Generic;

namespace StructuralPatterns.Proxy
{
    public class CachedUniversity : IUniversity
    {
        private readonly IUniversity _university;
        private IEnumerable<string> _cachedFaculties;
        
        public CachedUniversity(IUniversity university)
        {
            _university = university;
        }
        
        public IEnumerable<string> GetFaculties()
        {
            if (_cachedFaculties == null)
                _cachedFaculties = _university.GetFaculties();
            
            Console.WriteLine("Вызов прокси University!");
            return _cachedFaculties;
        }
    }
}