﻿using System.Collections.Generic;

namespace StructuralPatterns.Proxy
{
    public interface IUniversity
    {
        IEnumerable<string> GetFaculties();
    }
}