﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace StructuralPatterns.Proxy
{
    public class University : IUniversity
    {
        public IEnumerable<string> GetFaculties()
        {
            Console.WriteLine("Вызов изначального University!");
            Thread.Sleep(1000);  
            return new[] {"Механико-математический", "Физический", "Геологический"};
        }
    }
}